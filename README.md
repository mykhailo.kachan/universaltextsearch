### Example of calling the universalTextSearch from a parent component:
```html
<c-universal-text-search
	onrecord={onRecord}
	object-api-name={objectApiName}
	fields-to-query={fieldsToQuery}
	fields-to-display={fieldsToDisplay}
	fields-to-search-by={fieldsToSearchBy}
	title={title}
	placeholder={placeholder}
	is-required={isRequired}
	placeholder-empty-value={placeholderEmptyValue}
>
</c-universal-text-search>
```
### Example of parameters (* - Required):
```js
 *objectApiName  = 'Account';
 *fieldsToQuery = ['Name', 'Type', 'Phone', 'Rating', 'Fax'];
 *fieldsToDisplay = ['Name', 'Type', 'Phone'];
 *fieldsToSearchBy = ['Name', 'Type'];
 title = 'Search Account..';
 placeholder = 'Type to search ...';
 isRequired = true;
 placeholderEmptyValue = ' - ';
``` 
### Access to fields:
```html
onRecord(event) {
    this.idToDisplay = event.detail.Id;
    this.nameToDisplay = event.detail.Name;
    this.typeToDisplay = event.detail.Type;
}
```
##
#### Example UI
![UI](./images/1.png)
#### param. -> title
![title](./images/2.png)
#### param. -> placeholder
![placeholder](./images/3.png)
#### param. -> fields-to-display
![fields-to-display](./images/4.png)
#### param. -> placeholder-empty-value
![placeholder-empty-value](./images/5.png)
