public with sharing class UniversalTextSearchController {
    private static final Integer RECORDS_QUERY_LIMIT = 16;
    private static final Set<String> SEARCH_BY_TYPE = new Set<String> {'STRING', 'EMAIL', 'PHONE'};

    @AuraEnabled(cacheable=true)
    public static String getResults(String textForSearch, List<String> fieldsToQuery, List<String> fieldsToSearchBy, String objApiName) {
        List<SObject> resultObjects  = new  List<SObject>();
        List<String> confirmedFieldsToQuery = new List<String>();
        List<String> confirmedFieldsToSearchBy = new List<String>();
        Map<String, Schema.SObjectField> fieldsMap = new Map<String, Schema.SObjectField>() ;

        String searchText = String.escapeSingleQuotes(textForSearch.trim());
        String objectApiName = objApiName.trim();

        if (fieldsToQuery.size() < 1 || fieldsToSearchBy.size() < 1) {
            return null;
        }

        if( String.isBlank(searchText) ||  String.isBlank(objectApiName)){
            return null;
        }

        try {
            fieldsMap = Schema.getGlobalDescribe().get(objectApiName).getDescribe().fields.getMap();
            confirmedFieldsToQuery = isFieldsExistsAndTypeValid(fieldsToQuery, fieldsMap, false);
            confirmedFieldsToSearchBy = isFieldsExistsAndTypeValid(fieldsToSearchBy, fieldsMap, true);
            if (confirmedFieldsToQuery.size() < 1 || confirmedFieldsToSearchBy.size() < 1) {
                return null;
            }
            List<String> queryStrings = getFieldsToSearchBy(searchText, confirmedFieldsToSearchBy);
            resultObjects = Database.Query(getQueryString(confirmedFieldsToQuery, objectApiName, queryStrings));
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }

        return JSON.serialize(resultObjects);
    }

    private static  List<String> isFieldsExistsAndTypeValid(List<String> fieldNames, Map<String, Schema.SObjectField> fieldsMap, Boolean typeCheck) {
        Set<String> isExistsField = new Set<String>();

        for (String field : fieldNames) {
            if (typeCheck && fieldsMap.containsKey(field)) {
                if (typeCheck && SEARCH_BY_TYPE.contains((String) fieldsMap.get(field).getDescribe().getType().name())) {
                    isExistsField.add(field);
                }
            }else if(fieldsMap.containsKey(field)){
                isExistsField.add(field);
            }
        }

        return new List<String>(isExistsField) ;
    }

    private static String getQueryString( List<String> fieldsToQuery, String objectName, List<String> fieldsToSearchBy ) {
        return 'SELECT ' +
                String.join(fieldsToQuery, ', ') +
                ' FROM ' +
                objectName +
                ' WHERE'+
                ' ('+
                String.join(fieldsToSearchBy, ' OR ') +
                ') '+
                'LIMIT ' + RECORDS_QUERY_LIMIT;
    }

    private static List<String> getFieldsToSearchBy(String searchText, List<String> fieldsToSearchBy) {
        List<String> queryStrings = new List<String>();

        for (String field : fieldsToSearchBy) {
            queryStrings.add( ' ('+field+ ' LIKE \'' + searchText + '%\')');
        }

        return queryStrings;
    }
}
