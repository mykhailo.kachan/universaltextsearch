@isTest
public with sharing class UniversalTextSearchControllerTest {
    static final Integer TOTAL_CONTACT = 2;
    static final String LAST_NAME  = 'LName';
    static final String SEARCH_TEXT = 'LNa';
    static final String SEARCH_NOT_EXIST_TEXT = 'test';
    static final List<String> FIELDS_TO_QUERY = new List<String> {'Name', 'Phone', 'Email', 'Title'};
    static final List<String> FIELDS_TO_SEARCH_BY = new List<String> {'Name', 'Phone'};
    static final List<String> NOT_EXIST_FIELD = new List<String> {'notExField'};
    static final String OBJECT_API_NAME  = 'Contact';
    static final String NOT_EXIST_OBJECT_API_NAME  = 'NotContact';
    static final String AURA_HANDLED_EXC = 'Script-thrown exception';

    @TestSetup
    static void setup() {
        List<Contact> contacts = new List<Contact>();

        for (Integer i = 0; i < TOTAL_CONTACT ; i++) {
            contacts.add(new Contact( LastName = LAST_NAME, Title = 'Title', Phone = '123456789', Email = 'test@m.com' ));
        }
        for (Integer i = 0; i < TOTAL_CONTACT ; i++) {
            contacts.add(new Contact( LastName = 'SecondName', Title = 'Title', Phone = '123456789', Email = 'test@m.com' ));
        }
        insert contacts;
    }

    @isTest
    public static void getRecordsTest() {
        Test.startTest();
        String resultString = UniversalTextSearchController.getResults(SEARCH_TEXT, FIELDS_TO_QUERY, FIELDS_TO_SEARCH_BY, OBJECT_API_NAME);
        Test.stopTest();

        List<SObject> resultObjects = (List<SObject>) JSON.deserialize(resultString, List<SObject>.class);
        List<String> fieldsToSearchBy = getFieldsToSearchBy(SEARCH_TEXT, FIELDS_TO_SEARCH_BY);

        System.assertEquals(resultObjects.size(), Database.Query(getQueryString( FIELDS_TO_QUERY, OBJECT_API_NAME, fieldsToSearchBy )).size());
    }

    @isTest
    public static void searchNotExistingTextTest() {
        Test.startTest();
        String resultString = UniversalTextSearchController.getResults(SEARCH_NOT_EXIST_TEXT, FIELDS_TO_QUERY, FIELDS_TO_SEARCH_BY, OBJECT_API_NAME);
        Test.stopTest();

        List<SObject> resultObjects = (List<SObject>) JSON.deserialize(resultString, List<SObject>.class);
        List<String> fieldsToSearchBy = getFieldsToSearchBy(SEARCH_NOT_EXIST_TEXT, FIELDS_TO_SEARCH_BY);

        System.assertEquals(resultObjects.size(), Database.Query(getQueryString( FIELDS_TO_QUERY, OBJECT_API_NAME, fieldsToSearchBy )).size());
    }

    @isTest
    public static void notExistingFieldInQueryTest() {
        List<String> allFieldsToQuery = new List<String> (FIELDS_TO_QUERY );
        allFieldsToQuery.addAll(NOT_EXIST_FIELD);

        Test.startTest();
        String resultString = UniversalTextSearchController.getResults(SEARCH_TEXT, allFieldsToQuery, FIELDS_TO_SEARCH_BY, OBJECT_API_NAME);
        Test.stopTest();

        List<SObject> resultObjects = (List<SObject>) JSON.deserialize(resultString, List<SObject>.class);
        List<String> fieldsToSearchBy = getFieldsToSearchBy(SEARCH_TEXT, FIELDS_TO_SEARCH_BY);

        System.assertEquals(resultObjects.size(), Database.Query(getQueryString( FIELDS_TO_QUERY, OBJECT_API_NAME, fieldsToSearchBy )).size());
    }

    @isTest
    public static void withoutFieldsTest () {
        List<String> fieldsToQuery = new List<String> ();

        Test.startTest();
        String resultString = UniversalTextSearchController.getResults(SEARCH_TEXT, fieldsToQuery, FIELDS_TO_SEARCH_BY, OBJECT_API_NAME);
        Test.stopTest();

        System.assertEquals(resultString, null);
    }

    @isTest
    public static void notExistingObjectApiNameTest () {
        try{
            Test.startTest();
            UniversalTextSearchController.getResults(SEARCH_TEXT, FIELDS_TO_QUERY, FIELDS_TO_SEARCH_BY, NOT_EXIST_OBJECT_API_NAME);
            Test.stopTest();
        }catch (Exception e){

            System.assertEquals(e.getMessage(), AURA_HANDLED_EXC);

        }
    }

    private static List<String> getFieldsToSearchBy(String searchText, List<String> fieldsToSearchBy) {
        List<String> queryStrings = new List<String>();

        for (String field : fieldsToSearchBy) {
            queryStrings.add( ' ('+field+ ' LIKE \'' + searchText + '%\')');
        }

        return queryStrings;
    }

    private static String getQueryString( List<String> fieldsToQuery, String objectName, List<String> fieldsToSearchBy ) {
        return 'SELECT ' +
                String.join(fieldsToQuery, ', ') +
                ' FROM ' +
                objectName +
                ' WHERE'+
                ' ('+
                String.join(fieldsToSearchBy, ' OR ') +
                ') ';
    }
}
