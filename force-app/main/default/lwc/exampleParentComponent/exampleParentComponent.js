import { LightningElement, track} from 'lwc';

export default class UniversalTextSearchParent extends LightningElement {
    @track textToDisplay;
    @track idToDisplay;

    objectApiName  = 'Contact';
    fieldsToQuery = ['Name', 'Title', 'Phone'];
    fieldsToDisplay = ['Name', 'Title', 'Phone'];
    fieldsToSearchBy = ['Name'];
    title = '';
    placeholder = '';
    placeholderEmptyValue = '';

    objectApiName1  = 'Account';
    fieldsToQuery1 = ['Name', 'Type', 'Phone'];
    fieldsToDisplay1 = ['Name', 'Type', 'Phone'];
    fieldsToSearchBy1 = ['Name', 'Phone'];
    title1 = 'Search..';
    placeholder1 = 'Type to search ...';
    isRequired1 = true;
    placeholderEmptyValue1 = ' -- ';

    objectApiName2  = 'Car__c';
    fieldsToQuery2 = ['Brand__c', 'Cost__c', 'Color__c', '---'];
    fieldsToDisplay2 = ['Brand__c', 'Cost__c', 'Color__c'];
    fieldsToSearchBy2 = ['Brand__c', 'Color__c'];
    title2 = 'Search Car';
    placeholder2 = 'Type to search Car';
    isRequired2 = true;
    placeholderEmptyValue2 = ' - ';


    onRecord(event) {
        console.log('event '+event.detail)
        console.log('event '+event.detail.textToDisplay)
        this.textToDisplay = event.detail.textToDisplay;
        this.idToDisplay = event.detail.Id;
    }

    handleReset(){
        this.textToDisplay = '';
        this.idToDisplay = '';
    }

}

