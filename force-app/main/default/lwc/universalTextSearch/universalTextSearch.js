import {LightningElement, track, api} from 'lwc';
import getResults from '@salesforce/apex/UniversalTextSearchController.getResults';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';

export default class UniversalTextSearch extends LightningElement {
    MAX_NUMBER_RECORDS= 15;
    DELAY = 550;
    @api selectedRecord;
    @api objectApiName;
    @api fieldsToQuery;
    @api fieldsToDisplay;
    @api fieldsToSearchBy;
    @api isRequired = false;
    @api placeholder = 'Type to search...';
    @api title = 'Search';
    @api placeholderEmptyValue = ' - ';
    @track error;
    @track foundObjects;
    @track isLoading = false;
    @track searchText = '';
    @track moreThanMaxFields = false;
    @track isShowListbox = false;

    handleChangeInput(event) {
        this.searchText = event.target.value;
        if (!this.searchText) {
            this.handleInputClear();
            return;
        }else {
            event.target.reportValidity();
        }
        if (this.searchText.length < 2){
            return
        }
        this.isLoading = true;
        window.clearTimeout(this.delayTimeout);
        this.delayTimeout = setTimeout(() => {
            this.foundObjects = '';
        this.handleGetResults();
    }, this.DELAY);
    }

    handleGetResults() {
        this.moreThanMaxFields = false;
        getResults({
            textForSearch: this.searchText,
            fieldsToQuery: [ ...new Set([...this.fieldsToQuery,...this.fieldsToDisplay])],
            fieldsToSearchBy: this.fieldsToSearchBy,
            objApiName: this.objectApiName
    })
    .then(response  => {
            let result = JSON.parse(response );
        if(result !== null && result.length > 0){
            this.foundObjects = this.mergeFieldsToDisplay(result, this.fieldsToDisplay);
            this.moreThanMaxFields = this.foundObjects.length > this.MAX_NUMBER_RECORDS ? true : false;
            this.isShowListbox = true;
        }else {
            this.isShowListbox = false;
        }
        this.error ='';
        this.isLoading = false;
    })
    .catch(error => {
            this.isShowListbox = false;
        this.error = error.body.message;
        this.isLoading = false;
        this.showErrorToast(error.body.message, 'Search Error', 'error');
    });
    }

    mergeFieldsToDisplay(foundFields, fieldsToDisplay){
        let self= this;
        foundFields.forEach(function (field) {
            let tempText = [];
            fieldsToDisplay.forEach(fieldName => {
                tempText.push( field[fieldName] ? field[fieldName] : self.placeholderEmptyValue);
        })
            field.textToDisplay = tempText.join(' | ');
        });
        return foundFields;
    }

    handleSelectRecord(event){
        this.isShowListbox = false;
        this.selectedRecord = this.foundObjects.find( item => item.Id === event.currentTarget.dataset.rowIndex );
        this.searchText = this.selectedRecord.textToDisplay;
        this.foundObjects = '';
        this.handleChangeToParent();
    }

    handleInputClear(){
        this.isShowListbox = false;
        this.searchText = '';
        this.foundObjects = '';
    }

    showErrorToast(errorMessage, title, variant ){
        const evt = new ShowToastEvent({
            title: title,
            message: errorMessage,
            variant: variant,
            mode: 'dismissable'
        });
        this.dispatchEvent(evt)
    }

    handleChangeToParent() {
        const selectedEvent = new CustomEvent("record", {
            detail: this.selectedRecord
        });
        this.dispatchEvent(selectedEvent);
    }
}